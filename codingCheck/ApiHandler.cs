﻿using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace codingCheck
{
  internal class ApiHandler
  {
    static HttpClient client = new HttpClient();

    /// <summary>
    /// Fetch and parse data from the valet API based on user specified values
    /// </summary>
    /// <param name="conversionObject"></param>
    /// <returns>ApiObject if successfully fetched and parsed, null if not</returns>
    public async Task<ApiObject> FetchData(ConversionObject conversionObject)
    {
      string data = await FetchConversionRates(conversionObject.Path);

      if (data == null)
      {
        return null;
      }

      ApiObject apiObject = JsonSerializer.Deserialize<ApiObject>(data);

      if (apiObject.observations.Count == 0)
      {
        return null;
      }

      return apiObject;
    }

    /// <summary>
    /// Fetch data based on generated query from user input
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    private static async Task<string> FetchConversionRates(string path)
    {
      HttpResponseMessage response = await client.GetAsync(path);
      if (response.IsSuccessStatusCode)
      {
        string data = await response.Content.ReadAsStringAsync();
        return data;
      }

      return null;
    }
  }
}
