﻿using System.Threading.Tasks;

namespace codingCheck
{
  internal class Program
  {
    static async Task Main(string[] args)
    {
      await ConsoleController.RunProgram();
    }
  }
}
