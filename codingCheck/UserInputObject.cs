﻿using System.Collections.Generic;

namespace codingCheck
{
  public enum UserInput
  {
    Terminate,
    Help,

    Error,
    Success,
  }

  internal class UserInputObject
  {
    public UserInput InputResult { get; private set; }
    public ConversionObject Conversion { get; private set; }
    public List<string> ErrorMessages { get; private set; }

    public UserInputObject(UserInput inputResult, ConversionObject conversion, List<string> errorMessages)
    {
      InputResult = inputResult;
      Conversion = conversion;
      ErrorMessages = errorMessages;
    }

    /// <summary>
    /// User Input was valid, but not a conversion command
    /// </summary>
    /// <param name="inputResult"></param>
    public UserInputObject(UserInput inputResult) 
      : this(inputResult, null, null)
    {

    }

    /// <summary>
    /// User Input was a valid conversion command
    /// </summary>
    /// <param name="conversion"></param>
    public UserInputObject(ConversionObject conversion) 
      : this(UserInput.Success, conversion, null)
    {

    }

    /// <summary>
    /// User Input was incorrect and not understood
    /// </summary>
    /// <param name="errorMessages"></param>
    public UserInputObject(List<string> errorMessages)
      : this(UserInput.Error, null, errorMessages)
    {

    }
  }
}
