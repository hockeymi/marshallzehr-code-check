﻿using System.Collections.Generic;
using System.Text.Json;

namespace codingCheck
{
  internal class ApiObject
  {
    public object terms { private get; set; }
    public object seriesDetail { private get; set; }
    public IList<Dictionary<string, object>> observations { get; set; }

    public string Date => GetDate();
    public decimal Rate => GetRate();

    /// <summary>
    /// Get the date from the dictionary
    /// </summary>
    /// <returns></returns>
    private string GetDate()
    {
      return observations[0]["d"]?.ToString();
    }

    /// <summary>
    /// Get the exchange rate from the json object
    /// </summary>
    /// <returns></returns>
    private decimal GetRate()
    {
      // The generated query always limits to one record, or a single date
      foreach (var entry in observations[0])
      {
        // Valet API documentation says that the return format for an observation is:
        // {
        //   "d": "2022-06-01",
        //   "FXUSDCAD": {
        //     "v": "1.2639"
        //   }
        // }

        if (entry.Key == "d")
        {
          continue;
        }

        // There are probably safer ways to do this, but based on the valet API documentation, observation data should always be returned in this format
        string rate = JsonSerializer.Deserialize<Dictionary<string, string>>(entry.Value.ToString())["v"];

        return decimal.Parse(rate);
      }

      return decimal.MinusOne;
    }
  }
}
