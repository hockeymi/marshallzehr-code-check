﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace codingCheck
{
  internal static class ConsoleController
  {
    public async static Task RunProgram()
    {
      ApiHandler apiHandler = new ApiHandler();
      List<string> errorMessages = new List<string>();
      bool continueExecution = true;

      Console.WriteLine("Welcome to the super fancy currency converter!!!");
      Console.WriteLine();
      
      // Display the help info before prompting for user input
      DisplayHelp();

      while (continueExecution)
      {
        errorMessages.Clear();
        // Get user input
        UserInputObject userInput = GetUserInput(errorMessages);

        // Switch through all possible results of the user input
        switch (userInput.InputResult)
        {
          // Close the program
          case UserInput.Terminate:
            DisplayTerminate();
            continueExecution = false;
            break;

          // Display help info
          case UserInput.Help:
            DisplayHelp();
            break;

          // Error while parsing user input
          case UserInput.Error:
            DisplayInputErrors(errorMessages);
            break;

          // Parsing user input was successful
          case UserInput.Success:
            // Fetch data from valet API
            ApiObject apiObject = await apiHandler.FetchData(userInput.Conversion);

            // If null returned, there was an error while fetching/parsing data
            if (apiObject == null)
            {
              DisplayFetchError();
              break;
            }

            // Output results of user request
            DisplayQueryResult(userInput.Conversion, apiObject);
            break;
        }
      }
    }

    /// <summary>
    /// Read user input and parse accordingly
    /// </summary>
    /// <param name="errorMessages"></param>
    /// <returns></returns>
    private static UserInputObject GetUserInput(List<string> errorMessages)
    {
      Console.WriteLine("Enter conversion request:");
      Console.Write("> ");
      string args = Console.ReadLine();

      // Non-conversion commands are resolved here
      switch (args.ToLower())
      {
        case "exit":
        case "close":
          return new UserInputObject(UserInput.Terminate);

        case "help":
          return new UserInputObject(UserInput.Help);
      }

      // Attempt to parse the user input
      ConversionObject conversionObject = ConvertArgs(args, errorMessages);

      if (errorMessages.Count > 0)
      {
        return new UserInputObject(errorMessages);
      }

      return new UserInputObject(conversionObject);
    }

    /// <summary>
    /// Attempt to parse the user input into a conversion command
    /// </summary>
    /// <param name="args"></param>
    /// <param name="errorMessages"></param>
    /// <returns></returns>
    private static ConversionObject ConvertArgs(string args, List<string> errorMessages)
    {
      string[] argList = args.Split(' ', StringSplitOptions.RemoveEmptyEntries);

      // Assert that there are the mimimum required amount of args
      if (argList.Length < 3)
      {
        errorMessages.Add($"'{args}' is not a recognized command");
        errorMessages.Add($"type 'help' for more info");
        return null;
      }

      decimal amount;
      FromTo fromOrTo;
      string currency = argList[2];
      string date = string.Empty;

      // Parse the amount (Arg 1)
      if (!decimal.TryParse(argList[0], out amount))
      {
        errorMessages.Add("Your first argument must be a decimal value, with no commas or spaces");
      }

      // Parse the 'to' or 'from' (Arg 2)
      if (argList[1].ToLower() == "from")
      {
        fromOrTo = FromTo.From;
      }
      else if (argList[1].ToLower() == "to")
      {
        fromOrTo = FromTo.To;
      }
      else
      {
        // this is set only because 'fromOrTo' needs to be instantiated, even though it will not be used
        fromOrTo = FromTo.From;
        errorMessages.Add("Your second argument must be either the words 'from' or 'to'");
      }

      // Parse the foreign currency (Arg 3)
      // NOTE: Choosing to allow for currency codes to be lower case
      if (!ConversionObject.ACCEPTABLE_CURRENCIES.Contains(argList[2].ToUpper()))
      {
        errorMessages.Add("Your third argument must be a valid foreign currency code (ISO 4217)");
      }

      // Parse the date (Arg 4) - only if it was provided
      // NOTE: Choosing to not throw any errors if there are more than four arguments. Any extra arguments are ignored
      if (argList.Length >= 4)
      {
        date = argList[3];

        // NOTE: From what I could gather about the valet API, the first available dates for conversion rates begin in 2017
        // I'm also aware that not all dates since 2017 have any conversion rate data

        // This regex ensures that the provided date falls within:
        //  - year 2017 - 2022
        //  - month 01 - 12
        //  - day 01-31
        // It does not validate whether or not a given date actually exists (ie 2017-02-31)
        if (!Regex.IsMatch(date, @"20(1[7-9]|2[0-2])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])"))
        {
          errorMessages.Add("Your fourth argument must be a date in the format 'YYYY-MM-DD'");
        }
      }

      if (errorMessages.Count > 0)
      {
        return null;
      }

      return new ConversionObject(amount, fromOrTo, currency, date);
    }

    /// <summary>
    /// Write termination notification to console
    /// </summary>
    private static void DisplayTerminate()
    {
      Console.WriteLine("I'm sorry to see you go");
    }

    /// <summary>
    /// Write help info to console
    /// </summary>
    private static void DisplayHelp()
    {
      Console.WriteLine("This program will convert between foreign currencies and Canadian dollars, " +
        "and from Canadian dollars to foreign currencies");
      Console.WriteLine();
      Console.WriteLine("NOTE: 'exit' and 'close' will terminate the program, 'help' will display this menu again");
      Console.WriteLine();
      Console.WriteLine("Input args should separated by spaces, and are as follows:");
      Console.WriteLine(" - Arg 1: value of currency to be converted (decimal value, no commas or spaces)");
      Console.WriteLine(" - Arg 2: from/to");
      Console.WriteLine(" - Arg 3: foreign currency (ISO 4217)");
      Console.WriteLine(" - Arg 4: (optional) Date of exchange rate (YYYY-MM-DD)");
      Console.WriteLine();
      Console.WriteLine("example: 12345678.90 from USD 2022-02-02");
      Console.WriteLine();
    }

    /// <summary>
    /// Write error messages to console
    /// </summary>
    /// <param name="errorMessages"></param>
    private static void DisplayInputErrors(List<string> errorMessages)
    {
      foreach (string error in errorMessages)
      {
        Console.WriteLine(error);
      }

      Console.WriteLine();
    }

    /// <summary>
    /// Write fetch error to console
    /// </summary>
    private static void DisplayFetchError()
    {
      Console.WriteLine("There was an error while attempting to parse the respnose from the valet API");
      Console.WriteLine("This is generally due to the conversion rate being unavailable for the specified foreign currency on the specified date");
      Console.WriteLine();
    }

    /// <summary>
    /// Write query results to console
    /// </summary>
    /// <param name="conversionObject"></param>
    /// <param name="apiObject"></param>
    private static void DisplayQueryResult(ConversionObject conversionObject, ApiObject apiObject)
    {
      string fromCurrency = conversionObject.FromOrTo == FromTo.From ? conversionObject.Currency : "CAD";
      string toCurrency = conversionObject.FromOrTo == FromTo.To ? conversionObject.Currency : "CAD";
      decimal rate = conversionObject.ConvertedAmount(apiObject.Rate);

      Console.WriteLine($"{conversionObject.Amount} {fromCurrency} is worth {rate:0.0000} {toCurrency}");
      Console.WriteLine($"Rate: {apiObject.Rate}, Date: {apiObject.Date}");
      Console.WriteLine();
    }
  }
}
