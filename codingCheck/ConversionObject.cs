﻿namespace codingCheck
{
  public enum FromTo
  {
    From,
    To,
  }

  internal class ConversionObject
  {
    // This can be updated as needed. Values were chosen based on spec
    public static string[] ACCEPTABLE_CURRENCIES = new string[]
      {
      "USD",
      "EUR",
      "JPY",
      "GBP",
      "AUD",
      "CHF",
      "CNY",
      "HKD",
      "MXN",
      "INR",
      };

    public string Path => GetPath();
    public decimal Amount { get; private set; }
    public FromTo FromOrTo { get; private set; }
    public string Currency { get; private set; }
    public string Date { get; private set; }

    public decimal ConvertedAmount(decimal rate) => GetConvertedAmount(rate);

    public ConversionObject(decimal amount, FromTo fromOrTo, string currency, string date)
    {
      Amount = amount;
      FromOrTo = fromOrTo;
      Currency = currency.ToUpper();
      Date = date;
    }

    /// <summary>
    /// Generate path for api call
    /// </summary>
    /// <returns></returns>
    private string GetPath()
    {
      // Attempt to limit the query to only return 1 result
      string path = $"https://www.bankofcanada.ca/valet/observations/FX{Currency}CAD?";
      string date = (Date == string.Empty) ? "recent=1" : $"start_date={Date}&end_date={Date}";

      return $"{path}{date}";
    }

    /// <summary>
    /// Get the converted amount based on the conversion rate
    /// </summary>
    /// <param name="rate"></param>
    /// <returns></returns>
    private decimal GetConvertedAmount(decimal rate)
    {
      if (FromOrTo == FromTo.From)
      {
        return rate * Amount;
      }

      return Amount / rate;
    }
  }
}
